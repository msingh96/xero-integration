require('dotenv').config();
const fs = require('fs');
const express= require('express');
const jwtDecode = require('jwt-decode');
const {XeroClient }= require('xero-node');
const session = require('express-session');

//Note: When errors are encountered , error view is rendered to lead user to action instead of logging the error. Ideally we would want to look at what the error is but as this is small integration, 
//rendering a view prompting user to reconnect would fix the issue.

const app = express();

app.set('view engine', 'ejs');

const oneDay = 1000 * 60 * 60 * 24;
app.use(session({
	secret: 'sfeiljbnqcpoxifjcoo',
	resave: false,
	saveUninitialized: true,
	cookie: { maxAge: oneDay },
}));


const client_id = process.env.CLIENT_ID;
const client_secret = process.env.CLIENT_SECRET;
const redirectUrl = process.env.REDIRECT_URI;
const scopes = 'openid profile email accounting.settings accounting.reports.read accounting.journals.read accounting.contacts accounting.attachments accounting.transactions offline_access';

const xero = new XeroClient({
	clientId: client_id,
	clientSecret: client_secret,
	redirectUris: [redirectUrl],
	scopes: scopes.split(' '),
});

if (!client_id || !client_secret || !redirectUrl) {
	throw Error('Environment Variables not all set - please check your .env file in the project root or create one!')
}

app.get('/', async(req,res) => {
    res.render('index');  
})

app.get('/connect', async function (req, res) {
        try {
            const consentUrl = await xero.buildConsentUrl();
            res.redirect(consentUrl);
        } catch (err) {
            res.render('error');
        }
})

app.get('/home', async(req,res) => {
    res.render("links");
})

app.get('/callback', async (req, res) => {
    try {
        //grabbing info from callback and setting into session
        const tokenSet = await xero.apiCallback(req.url);
        await xero.updateTenants();
        
        const decodedIdToken = jwtDecode(tokenSet.id_token);
        const decodedAccessToken = jwtDecode(tokenSet.access_token);

        req.session.decodedIdToken = decodedIdToken;
        req.session.decodedAccessToken = decodedAccessToken;
        req.session.tokenSet = tokenSet;   

        req.session.allTenants = xero.tenants;
        req.session.activeTenant = xero.tenants[0];
        res.render('links');
    } catch (err) {
        res.render('error');
    }
});


app.get("/contacts", async(req,res) => {
    try{
        const response  = await xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
        fs.writeFile('contacts.json', JSON.stringify(response.body,null,4), (err) => {
            if(err){
                console.log(err)
            }else{
                console.log("file written")
            }
        });
        res.render("result");
    }
    catch(err){
        res.render('error');
    }
    
})

app.get('/accounts', async(req,res) => {

    try {
        const response = await xero.accountingApi.getAccounts(req.session.activeTenant.tenantId);
        fs.writeFile('accounts.json', JSON.stringify(response.body,null,4), (err) => {
            if(err){
                console.log(err)
            }else{
                res.render('result');
            }
        });
      } catch (err) {
        //const error = JSON.stringify(err.response, null, 2)
        res.render('error');
    }
})

app.listen(8080, () => {
    console.log("Listening on 8080");
})